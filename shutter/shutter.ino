/* shutter.ino
 * 
 * Firmware for a simple servo-controlled shutter.
 * 
 * Copyright (c) 2020 by Jann Eike KRUSE
 * 
 * This program is Free Software, since it is 
 * licensed under the GNU AGPL 3.0 licence. 
 * 
 */

const char VERSION_STRING[] = "v1.0RC2";


#include <Servo.h>

Servo shutter1;  // create object to control the servo of shutter 1
Servo shutter2;  // create object to control the servo of shutter 2
Servo shutter3;  // create object to control the servo of shutter 3

String command;

const uint8_t VERBOSITY = 3; // 0: quiet (protocol only), 1: Normal (Errors), 2: Verbose (Warnings), 3: Debug (Info)

enum Pins {
  PIN_SIGNAL1    = A0,
  PIN_FEEDBACK1  = A1,
  PIN_SIGNAL2    = A2,
  PIN_FEEDBACK2  = A3,
  PIN_SIGNAL3    = A4,  
  PIN_FEEDBACK3  = A5
};

int OPENED_POSITION1 = 0;
int CLOSED_POSITION1 = 180;

int OPENED_POSITION2 = 0;
int CLOSED_POSITION2 = 180;

int OPENED_POSITION3 = 0;
int CLOSED_POSITION3 = 180;

int message(uint8_t level, String text) {
  if (VERBOSITY >= level) Serial.print(text);
}

void setup() {
  Serial.begin(9600);
  shutter1.attach(PIN_SIGNAL1);  // attaches the servo on pin A0 to the Servo object
  shutter2.attach(PIN_SIGNAL2);  // attaches the servo on pin A0 to the Servo object
  shutter3.attach(PIN_SIGNAL3);  // attaches the servo on pin A0 to the Servo object
  
  message(1,"Starting ServoShutter version: ");
  message(1,VERSION_STRING);
  message(1,"\n");
  
  message(3,"Compiled on: ");
  message(3,__DATE__);
  message(3," - ");
  message(3,__TIME__);
  message(3,"\n");
  
  Serial.setTimeout(2147483647);  // Basically DON'T time out!
  while (Serial.available()) Serial.read(); // discard any previous data from serial port

  shutter1.write(CLOSED_POSITION1);
  shutter2.write(CLOSED_POSITION2);
  shutter3.write(CLOSED_POSITION3);
  
}

void loop() {
  command = Serial.readStringUntil(13);

  if (command == "test") Serial.print("OK");

  else if (command == "OPEN 1")  shutter1.write(OPENED_POSITION1);
  else if (command == "OPEN 2")  shutter2.write(OPENED_POSITION2);
  else if (command == "OPEN 3")  shutter3.write(OPENED_POSITION3);
  
  else if (command == "CLOSE 1") shutter1.write(CLOSED_POSITION1);
  else if (command == "CLOSE 2") shutter2.write(CLOSED_POSITION2);
  else if (command == "CLOSE 3") shutter3.write(CLOSED_POSITION3);
  
  else if (command == "READ 1") Serial.println(analogRead(PIN_FEEDBACK1));
  else if (command == "READ 2") Serial.println(analogRead(PIN_FEEDBACK2));
  else if (command == "READ 3") Serial.println(analogRead(PIN_FEEDBACK3));
  
  else {
    message(1,"ERROR - Unknown command: ");
    message(1,command);
    message(1,"\n");
  }
  delay(10);
  while (Serial.available()) Serial.read(); // discard other data
}
